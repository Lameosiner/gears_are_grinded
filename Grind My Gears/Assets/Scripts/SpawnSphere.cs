﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnSphere : MonoBehaviour
{
    public GameObject spherePrefab;
    public Text countdown;
    public float countdownTime;

    GameObject sphere;
    Transform spawnLocation;

    float currCountdownValue;


    void Start()
    {
        spawnLocation = GetComponent<Transform>();

        // Spawn at start of game
        SpawnsSphere();


    }

   
    public void SpawnsSphere()
    {


        sphere = Instantiate(spherePrefab, spawnLocation);
        sphere.GetComponent<Rigidbody>().useGravity = false;

        StartCoroutine(DropSphere());

        // UI
        StartCoroutine(StartCountdown(countdownTime));
       
    }
    
    IEnumerator DropSphere()
    {
        yield return new WaitForSeconds(3f);

        sphere.GetComponent<Rigidbody>().useGravity = true;
    }

    // UI
    IEnumerator StartCountdown(float countdownValue)
    {
        currCountdownValue = countdownValue;
        while (currCountdownValue > 0)
        {
            countdown.text = "Drops in: " + currCountdownValue;

            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }

        if (currCountdownValue == 0)
            countdown.text = " ";

    }

}
