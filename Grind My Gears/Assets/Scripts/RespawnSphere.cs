﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnSphere : MonoBehaviour
{

    public SpawnSphere respawn;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Sphere")
        { 
            Destroy(other.gameObject);
            respawn.SpawnsSphere();

            UIManager.instance.WinOrLoseState();
            UIManager.instance.gameOver = true;
            
        }
    }
}
