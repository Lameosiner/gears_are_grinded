﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearRotation : MonoBehaviour
{

    public float rotationSpeed;
    public bool isActive = false;

    // Start is called before the first frame update
    void Start()
    {
        isActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        Rotation();

        //if (isActive == true)
        //{
        //    Rotation();
        //}

    }

    void Rotation()
    {
        if (Input.GetButton("PlayerOneLeft") && gameObject.CompareTag("PlayerOne"))
        {
            //GetComponent<Rigidbody>().MoveRotation(Quaternion.Euler(rotationSpeed + transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
            transform.Rotate(0, rotationSpeed, 0);
        }

        if (Input.GetButton("PlayerOneRight") && gameObject.CompareTag("PlayerOne"))
        {
            //GetComponent<Rigidbody>().MoveRotation(Quaternion.Euler(transform.rotation.eulerAngles.x, rotationSpeed + transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
            transform.Rotate(0, -rotationSpeed, 0);
        }

        if (Input.GetButton("PlayerOneLeft") && gameObject.CompareTag("PlayerOneReverse"))
        {
            //GetComponent<Rigidbody>().MoveRotation(Quaternion.Euler(rotationSpeed + transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
            transform.Rotate(0, -rotationSpeed, 0);
        }

        if (Input.GetButton("PlayerOneRight") && gameObject.CompareTag("PlayerOneReverse"))
        {
            //GetComponent<Rigidbody>().MoveRotation(Quaternion.Euler(transform.rotation.eulerAngles.x, rotationSpeed + transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
            transform.Rotate(0, rotationSpeed, 0);
        }

        if (Input.GetButton("PlayerTwoLeft") && gameObject.CompareTag("PlayerTwo"))
        {
            transform.Rotate(0, rotationSpeed, 0);
        }

        if (Input.GetButton("PlayerTwoRight") && gameObject.CompareTag("PlayerTwo"))
        {
            transform.Rotate(0, -rotationSpeed, 0);
        }

        if (Input.GetButton("PlayerTwoLeft") && gameObject.CompareTag("PlayerTwoReverse"))
        {
            transform.Rotate(0, -rotationSpeed, 0);
        }

        if (Input.GetButton("PlayerTwoRight") && gameObject.CompareTag("PlayerTwoReverse"))
        {
            transform.Rotate(0, rotationSpeed, 0);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        isActive = false;
    }
}