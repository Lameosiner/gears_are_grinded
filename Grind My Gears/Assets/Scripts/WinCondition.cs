﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinCondition : MonoBehaviour
{

    public string sceneToLoad;

    private void OnTriggerEnter(Collider Sphere)
    {
        UIManager.instance.WinOrLoseState();

        SceneManager.LoadScene(sceneToLoad);
    }
}
