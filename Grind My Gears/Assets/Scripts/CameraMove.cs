﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    public Camera mainCam;
    

    void Start()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        mainCam = Camera.main;
        
    }


    void LateUpdate()
    {
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        Vector3 spherePosition = new Vector3(mainCam.transform.position.x, transform.position.y - 4, mainCam.transform.position.z);
        
        mainCam.transform.position = spherePosition;
    }
}
