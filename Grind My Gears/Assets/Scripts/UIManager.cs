﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    #region UI 

    public Text Timer;

    public List<Image> InputButtons;

    //0 = blue, 1 = bluePressed, 2 = red, 3 = redPressed
    public List<Color> ButtonColours;

    #endregion

    #region Variables

    public float currentTimerValue;
    public float maxTimer;

    public bool gameOver;

    #endregion

    void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        //Set current timer to max value
        currentTimerValue = maxTimer;

       // InvokeRepeating("TweenTimer", maxTimer - 5.5f, 1);
      
    }

    void Start()
    {
        #region Update UI Button Colour
        InputButtons[0].color = ButtonColours[0];
        InputButtons[1].color = ButtonColours[0];
        InputButtons[2].color = ButtonColours[2];
        InputButtons[3].color = ButtonColours[2];
        #endregion

    }

    void Update()
    {
        StartCoroutine(StartTimer());

        //Print only the first numbers of the float value
        Timer.text = currentTimerValue.ToString("f0");

        ChangeUIColour();


        if (currentTimerValue <= 0)
        {
            gameOver = true;
        }

        if (gameOver == true)
        {

            foreach(GameObject sphere in GameObject.FindGameObjectsWithTag("Sphere"))
            {
                Destroy(sphere.gameObject);
            }
 

          GameObject.FindGameObjectWithTag("Spawner").GetComponent<SpawnSphere>().SpawnsSphere();
          WinOrLoseState();
          gameOver = false;
        }

    }

    //Either if you advance levels, or if you die
    public void WinOrLoseState()
    {
        StopAllCoroutines();
        ResetTimer();
    }

    //Upon switching scenes, reset current timer to max
    void ResetTimer()
    {
        currentTimerValue = maxTimer;
        StartCoroutine(StartTimer());
    }

    IEnumerator StartTimer()
    {
        yield return new WaitForSeconds(3);
        currentTimerValue -= Time.deltaTime;
    }

    //Change the colour of the buttons when there is input
    void ChangeUIColour()
    {

        //PLAYER 1

        //Left
        if (Input.GetButton("PlayerOneLeft"))
        {
            InputButtons[0].color = ButtonColours[1];
        }

        if (Input.GetButtonUp("PlayerOneLeft"))
        {
            InputButtons[0].color = ButtonColours[0];
        }

        //Right
        if (Input.GetButton("PlayerOneRight"))
        {
            InputButtons[1].color = ButtonColours[1];
        }

        if (Input.GetButtonUp("PlayerOneRight"))
        {
            InputButtons[1].color = ButtonColours[0];
        }

        //PLAYER 2

        //Left
        if (Input.GetButton("PlayerTwoLeft"))
        {
            InputButtons[2].color = ButtonColours[3];
        }

        if (Input.GetButtonUp("PlayerTwoLeft"))
        {
            InputButtons[2].color = ButtonColours[2];
        }

        //Right
        if (Input.GetButton("PlayerTwoRight"))
        {
            InputButtons[3].color = ButtonColours[3];
        }

        if (Input.GetButtonUp("PlayerTwoRight"))
        {
            InputButtons[3].color = ButtonColours[2];
        }


    }

    //void TweenTimer()
    //{
    //    //If the timer drops below a certain value, punch
    
    //        Timer.transform.DOPunchScale(new Vector3(0.25f, 0.25f, 0.25f), 1, 1, 0);
        
    //}


}
